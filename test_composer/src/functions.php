<?php
function dino()
{
    $response = Requests::get("https://allosaurus.delahayeyourself.info/api/dinosaurs/");
    return json_decode($response->body);
}

function dinosolo($name)
{
    $uri = sprintf("https://medusa.delahayeyourself.info/api/dinosaurs/%s", $name);
    $response = Requests::get($uri);
    return json_decode($response->body);
}


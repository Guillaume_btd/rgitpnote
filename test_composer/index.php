<?php
require "vendor/autoload.php";

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::route('/', function(){
    $data = [
        'dinau' => dino(),
    ];
    Flight::render('ind.twig', $data);
});


Flight::route('/dinosaur/@name', function($name){
    $data = [
        'dino2' => dinosolo($name),
    ];
    Flight::render('dinosolo.twig', $data);
});

Flight::start();
?>